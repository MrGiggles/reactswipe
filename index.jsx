/// <reference path="typings/index.d.ts" />

import * as React from "react";
import * as ReactDOM from "react-dom";
import Swipeable from "react-swipeable";

class SwipeComponent extends React.Component {

    constructor(props){
        super(props);
    
        this.state = {
            left: -1 * (this.props.width - this.props.offset),
            lastLeft: -300,
        }
        
        this.swipingLeft = (e, x) => {
            this.setState({
                left: this.state.lastLeft - x,
            });
        }
        
        this.swipingRight = (e, x) => {
            this.setState({
                left: this.state.lastLeft + x,
            });
        }
        
        this.swipedLeft = (e, x, isFlick) => {
            this.setState({
                left: this.state.lastLeft - x,
                lastLeft: this.state.lastLeft - x,
            });
        }
        
        this.swipedRight = (e, x, isFlick) => {
            this.setState({
                left: this.state.lastLeft - x,
                lastLeft: this.state.lastLeft - x,
            });
        }
        
    }


    render(){
        return (
            <Swipeable
                onSwiping={this.swiping}
                onSwipingUp={this.swipingUp}
                onSwipingRight={this.swipingRight}
                onSwipingDown={this.swipingDown}
                onSwipingLeft={this.swipingLeft}
                onSwipedUp={this.swipedUp}
                onSwipedRight={this.swipedRight}
                onSwipedDown={this.swipedDown}
                onSwipedLeft={this.swipedLeft}
                onSwiped={this.handleSwipeAction}>
                    <div style={{
                        backgroundColor: "green",
                        width:"350px",
                        height: "100%",
                        position: "absolute",
                        top: "0px",
                        left: this.state.left + "px",
                        velocity: this.state.velocity + "px",
                    }}>
                        swipe here!<br/>
                        swipe here!<br/>
                        swipe here!<br/>
                        swipe here!<br/>
                        swipe here!<br/>
                        swipe here!<br/>
                        swipe here!<br/>
                </div>
            </Swipeable>
        );
    }

};

SwipeComponent.defaultProps = {
    width: 350,
    offset: 20,
}

ReactDOM.render(
    <SwipeComponent />,
    document.getElementById("node")
);

