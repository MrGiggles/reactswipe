 module.exports = {
     entry: './index.jsx',
     output: {
         path: './',
         filename: 'index.bundle.js',
     },
     module: {
         loaders: [{
             test: /\.jsx?$/,
             exclude: /node_modules/,
             loader: 'babel-loader'
         }]
     }
 }